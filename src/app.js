import dotenv from 'dotenv';
dotenv.config();

import { discordHandler } from './discordHandler.js';

if (!process.env.OPENAI_API_KEY || !process.env.DISCORD_TOKEN) {
    console.error('Missing required environment variables. Please check OPENAI_API_KEY and DISCORD_TOKEN.');
    process.exit(1);
}

discordHandler();

// Determine and log error details
function logErrorDetails(error) {
    if (error.message && error.code) {
        console.error(`Error: ${error.message}, Code: ${error.code}`);
    } else if (error.message) {
        console.error(`Error: ${error.message}`);
    } else if (error.code) {
        console.error(`Error Code: ${error.code}, Error: ${error}`);
    } else {
        console.error('Error:', error);
    }
}

// Universal error handler for uncaught exceptions
process.on('uncaughtException', (error) => {
    logErrorDetails(error);
});

// Universal error handler for unhandled promise rejections
process.on('unhandledRejection', (reason) => {
    if (reason instanceof Error) {
        logErrorDetails(reason);
    } else {
        console.error('Unhandled Rejection:', reason);
    }
});
