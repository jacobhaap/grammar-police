import dotenv from 'dotenv';
dotenv.config();

import OpenAI from 'openai';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY,
});

const sysPrompt = fs.readFileSync(path.join(__dirname, 'systemPrompt.txt'), 'utf8');

async function correctGrammar(text) {
    try {
        // Process the text through OpenAI API
        const chatCompletion = await openai.chat.completions.create({
            model: "gpt-3.5-turbo",
            messages: [
                { role: 'system', content: sysPrompt },
                { role: 'user', content: text },
            ],
            temperature: 0.1,
            max_tokens: 256,
            top_p: 1,
            frequency_penalty: 0,
            presence_penalty: 0,
        });

        // Check for valid response
        if (!chatCompletion || !chatCompletion.choices || chatCompletion.choices.length === 0 || !chatCompletion.choices[0].message) {
            console.error('Failed to receive a valid response from OpenAI:', chatCompletion);
            return null;
        }

        return chatCompletion.choices[0].message.content.trim();
    } catch (error) {
        console.error('An error occurred while calling OpenAI API:', error);
        return null;
    }
}

export default correctGrammar;
