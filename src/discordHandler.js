import dotenv from 'dotenv';
dotenv.config();

import { Client, GatewayIntentBits, REST, Routes, ApplicationCommandOptionType } from 'discord.js';
import correctGrammar from './correctGrammar.js';

const token = process.env.DISCORD_TOKEN;

export function discordHandler() {
    const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });

    client.once('ready', async () => {
        console.log('The Grammar Police are Ready!');
    
        const commands = [
            {
                name: 'grammar',
                description: 'Corrects the grammar of the input text',
                options: [
                    {
                        type: ApplicationCommandOptionType.String,
                        name: 'text',
                        description: 'The text you want to correct',
                        required: true,
                    },
                ],
            },
        ];
    
        const rest = new REST({ version: '10' }).setToken(token);
    
        try {
            console.log('Started refreshing application (/) commands.');
    
            await rest.put(
                Routes.applicationCommands(client.application.id),
                { body: commands },
            );
    
            console.log('Successfully reloaded application (/) commands globally.');
        } catch (error) {
            console.error(error);
        }
    });

    client.on('messageCreate', async message => {
        // Ignore bot messages and messages that are not replies or do not mention the bot
        if (message.author.bot || !message.mentions.has(client.user) || !message.reference) return;

        try {
            // Fetch the original message that was replied to
            const originalMessage = await message.channel.messages.fetch(message.reference.messageId);
            
            // Extract the text from the original message
            const textToCorrect = originalMessage.content.trim();
            
            // Process with correctGrammar.js
            const correctedText = await correctGrammar(textToCorrect);

            if (!correctedText) {
                message.reply('Failed to process the request.');
                return;
            }

            message.reply(`:rotating_light: **${originalMessage.author}! You are under the rest!** :rotating_light:\n\nWhat I believe you meant to say, is "${correctedText}"`);
        } catch (error) {
            console.error('An error occurred while calling OpenAI API:', error);
            message.reply('Sorry, an error occurred while processing your request.');
        }
    });

    client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand() || interaction.commandName !== 'grammar') return;

        await interaction.deferReply();

        // Extract the text from the command
        const textToCorrect = interaction.options.getString('text', true);

        try {
            // Process with correctGrammar.js
            const correctedText = await correctGrammar(textToCorrect);

            if (!correctedText) {
                await interaction.editReply('Failed to process the request.');
                return;
            }

            await interaction.editReply(`**I have prepared a statement on behalf of ${interaction.user}:** _${correctedText}_`);
        } catch (error) {
            console.error('An error occurred while calling OpenAI API:', error);
            await interaction.editReply('Sorry, an error occurred while processing your request.');
        }
    });

    client.login(token);
}
