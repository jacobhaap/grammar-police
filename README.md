## Grammar Police

> *I am a law enforcement officer entrusted with the duty to protect and*
> *serve the people of Discord from bad grammar. - Grammar Police*

Grammar Police is a Discord Bot using `discord.js` and `openai` to correct the grammar of a user's message. To call the bot, reply to a message and tag the bot in the reply, and the bot will correct the grammar of the message being replied to. Responses with corrected grammar are based on the `gpt-3.5-turbo` model.
